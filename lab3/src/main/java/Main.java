import java.util.*;

public class Main {
    public static void main(String[] args) {
        Integer[] numbers = {5, 9, 3, 2, 0, -4, -8, 3, -4};
        Arrays.sort(numbers);
        System.out.println("Max number " + numbers[numbers.length-1]);


        Integer[] n2 = {1, 6, 3, 3, 4, 5, 5, 6, 0};
        SortedSet<Integer> s = new TreeSet<>();
        Collections.addAll(s, n2);
        System.out.print(s);

        String[] arr = {"12", "23", "34", "12", "56", "67"};
        List<String> li = new ArrayList<>();
        li = Arrays.asList(arr);
        Collections.replaceAll(li, "12", "replaced");
        System.out.println(li);
    }
}
