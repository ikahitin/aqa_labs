import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import org.apache.log4j.Logger;

public class BaseTestListener implements ITestListener {

    private final Logger LOGGER = Logger.getLogger(this.getClass());

    @Override
    public void onTestStart(ITestResult result) {
        LOGGER.info("Start test");
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        LOGGER.info("Test was successful");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        LOGGER.warn("Test failed");
    }

    @Override
    public void onStart(ITestContext context) {
        LOGGER.info("Start");
    }

    @Override
    public void onFinish(ITestContext context) {
        LOGGER.info("Test was finished");
    }
}
